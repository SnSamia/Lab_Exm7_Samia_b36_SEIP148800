<?php
namespace App\ProfilePicture;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;



class ProfilePicture extends DB{
    public $id="";
    public $name="";
    public $profile_picture="";

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariableData=null){
        if(array_key_exists("id",$postVariableData)){
            $this->id=$postVariableData['id'];
        }
        if(array_key_exists("name",$postVariableData)){
            $this->name=$postVariableData['name'];
        }
        /*if(array_key_exists("picture_picture",$postVariableData)){
            $this->profile_picture=time().$_FILES['picture_picture']['name'];
        }*/
        $this->profile_picture=time().$_FILES['image']['name'];
        $temporary_location=$_FILES['image']['tmp_name'];

        move_uploaded_file($temporary_location,"../../../image/".$this->profile_picture);
    }

    public function store()
    {

        $arrData=array($this->name,$this->profile_picture);
        $sql="insert into profile_picture(name,image)VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result){
            Message::message("Success!Data has been inserted successfully");
        }
        else{
            Message::message("Failed!Data has not been inserted successfully");
        }

        Utility::redirect('create.php');



    }
}
?>

