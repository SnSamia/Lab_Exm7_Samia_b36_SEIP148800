<?php
namespace App\Gender;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;
class Gender extends DB
{
    public $id = "";
    public $name = "";
    public $gender = "";

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($postVariableData=null){
        if(array_key_exists("id",$postVariableData)){
            $this->id=$postVariableData['id'];
        }
        if(array_key_exists("name",$postVariableData)){
            $this->name=$postVariableData['name'];
        }
        if(array_key_exists("gender",$postVariableData)){
            $this->gender=$postVariableData['gender'];
        }

    }

    public function store(){
        $arrData=array($this->name,$this->gender);
        $sql="insert into gender(name	,gender)VALUES(?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result){
            Message::message("Success!Data has been inserted successfully");
        }
        else{
            Message::message("Failed!Data has not been inserted successfully");
        }

        Utility::redirect('create.php');
    }



}

?>

