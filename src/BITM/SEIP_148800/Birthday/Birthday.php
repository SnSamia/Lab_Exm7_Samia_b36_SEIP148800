<?php
namespace App\Birthday;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Birthday extends DB{
    public $id="";
    public $name="";
    public $birth_day="";

    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION))
            session_start();
    }

    public function setData($postVariableData=null){
        if(array_key_exists("id",$postVariableData)){
            $this->id=$postVariableData['id'];
        }
        if(array_key_exists("name",$postVariableData)){
            $this->name=$postVariableData['name'];
        }
        if(array_key_exists("date",$postVariableData)){
            $this->birth_day=$postVariableData['date'];
        }
    }

    public function store()
    {
        $arrData = array($this->name, $this->birth_day);
        $sql = "insert into birthday(name,birth_date)VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result){
            Message::message("Success!Data has been inserted successfully");
        }
        else{
            Message::message("Failed!Data has not been inserted successfully");
        }

        Utility::redirect('create.php');

    }




}

?>