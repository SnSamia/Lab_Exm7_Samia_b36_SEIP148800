<?php
namespace App\City;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
class City extends DB{
    public $id="";
    public $name="";
    public $city="";

    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION))
            session_start();
    }

    public function setData($postVariableData=null){
        if(array_key_exists("id",$postVariableData)){
            $this->id=$postVariableData['id'];
        }
        if(array_key_exists("name",$postVariableData)){
            $this->name=$postVariableData['name'];
        }
        if(array_key_exists("city_name",$postVariableData)){
            $this->city=$postVariableData['city_name'];
        }

    }

    public function store(){
        $arrData=array($this->city,$this->name);
        $sql="insert into city(city_name,name)VALUES(?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result){
            Message::message("Success!Data has been inserted successfully");
        }
        else{
            Message::message("Failed!Data has not been inserted successfully");
        }

        Utility::redirect('create.php');
    }


}

?>