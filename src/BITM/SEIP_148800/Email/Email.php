<?php
namespace App\Email;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;
class Email extends DB{
    public $id="";
    public $name="";
    public $email="";

    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION))
            session_start();
    }
    public function setData($postVariableData=null){
        if(array_key_exists("id",$postVariableData)){
            $this->id=$postVariableData['id'];
        }
        if(array_key_exists("name",$postVariableData)){
            $this->name=$postVariableData['name'];
        }
        if(array_key_exists("email",$postVariableData)){
            $this->email=$postVariableData['email'];
        }

    }

    public function store(){
        $arrData=array($this->name,$this->email);
        $sql="insert into email(name,email)VALUES(?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result){
            Message::message("Success!Data has been inserted successfully");
        }
        else{
            Message::message("Failed!Data has not been inserted successfully");
        }

        Utility::redirect('create.php');
    }


}

?>