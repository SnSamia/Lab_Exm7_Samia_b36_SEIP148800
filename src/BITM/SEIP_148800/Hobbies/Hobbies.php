<?php
namespace App\Hobbies;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class Hobbies extends DB
{
    public $id;
    public $name;
    public $hobbies=array();
    public $hobby;


    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION))
            session_start();
    }
    public function setData($postVaribaleData=NULL)
    {


        if(array_key_exists("id",$postVaribaleData))
        {
            $this->id = $postVaribaleData['id'];
        }
        if(array_key_exists("name",$postVaribaleData))
        {
            $this->name = $postVaribaleData['name'];
        }
        for ($i=1;$i<=5;$i++)
        {
            if(array_key_exists("hobby".$i,$postVaribaleData))
            {
                $this->hobby = $postVaribaleData['hobby'.$i];
                array_push($this->hobbies,$this->hobby);
            }
        }

        $this->hobby2=implode(',',$this->hobbies);

    }//end of set data
    public function store()
    {
        $arrData = array($this->name,$this->hobby2);
        $sql = "insert into hobbies(name,hobbies) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);

        if($result) {
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        }
        else {
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");
        }

        Utility::redirect('create.php');


    }

}