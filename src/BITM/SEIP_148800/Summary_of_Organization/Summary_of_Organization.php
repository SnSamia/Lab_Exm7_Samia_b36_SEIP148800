<?php
namespace App\Summary_of_Organization;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;

class Summary_of_Organization extends DB{
    public $id="";
    public $org_name="";
    public $org_summary="";

    public function __construct(){
        parent::__construct();
    }
    public function setData($postVariableData=null){
        if(array_key_exists("id",$postVariableData)){
            $this->id=$postVariableData['id'];
        }
        if(array_key_exists("org_name",$postVariableData)){
            $this->org_name=$postVariableData['org_name'];
        }
        if(array_key_exists("org_summary",$postVariableData)){
            $this->org_summary=$postVariableData['org_summary'];
        }
    }

    public function store()
    {
        $arrData = array($this->org_name, $this->org_summary);
        $sql = "insert into summary_of_organization(organization_name,comment)VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result){
            Message::message("Success!Data has been inserted successfully");
        }
        else{
            Message::message("Failed!Data has not been inserted successfully");
        }

        Utility::redirect('create.php');

    }
}
?>
