<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class BookTitle extends DB
    {
        public $id = " ";
        public $book_title = " ";
        public $author_name = " ";


        public function  __construct()
        {
            parent::__construct();

        }


        public function  setData($postVariableData = Null)
        {
            if (array_key_exists("id", $postVariableData)) {
                $this->id = $postVariableData['id'];


            }
            if (array_key_exists("book_title", $postVariableData)) {
                $this->book_title = $postVariableData['book_title'];
            }
            if (array_key_exists("author_name", $postVariableData)) {
                $this->author_name = $postVariableData['author_name'];
            }
        }
        public function store()
        {
            $arrData = array($this->book_title, $this->author_name);
            $sql = "insert into book_title(book_title,author_name)VALUES (?,?)";
            $STH=$this->DBH->prepare($sql);
            $result=$STH->execute($arrData);
            if($result){
                Message::message("Success!Data has been inserted successfully");
            }
            else{
                Message::message("Failed!Data has not been inserted successfully");
            }

            Utility::redirect('create.php');

        }




        public function index($fetchMode='ASSOC'){


            $STH = $this->DBH->query('SELECT * from book_title ORDER BY id ASC ');

            $fetchMode = strtoupper($fetchMode);
            if(substr_count($fetchMode,'OBJ') > 0)
                $STH->setFetchMode(PDO::FETCH_OBJ);
            else
                $STH->setFetchMode(PDO::FETCH_ASSOC);

            $arrAllData  = $STH->fetchAll();
            return $arrAllData;


        }// end of index();

        public function view($fetchMode='ASSOC'){

            $sql = 'SELECT * from book_title where id='.$this->id;

            $STH = $this->DBH->query($sql);

            $fetchMode = strtoupper($fetchMode);
            if(substr_count($fetchMode,'OBJ') > 0)
                $STH->setFetchMode(PDO::FETCH_OBJ);
            else
                $STH->setFetchMode(PDO::FETCH_ASSOC);

            $arrOneData  = $STH->fetch();
            return $arrOneData;


        }// end of view




    }

    ?>