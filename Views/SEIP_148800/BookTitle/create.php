<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

echo Message::message();
?>







<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">



    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resourse/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resourse/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resourse/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../resourse/assets/css/style.css">




</head>

<body>
<style>
    body{
        padding-top: 10px;

        background: url("../../../resourse/assets/img/backgrounds/books.jpg") no-repeat center center fixed;

        background-size:  cover;

    }
</style>


<div class="top-content">

    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text">
                    <h1><strong>ADD BOOK TITLE  :)</strong> </h1>
                    <div class="description">

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box">
                    <div class="form-top">
                        <div class="form-top-left">

                            <p>Enter Book Title and And Author Name:</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-book"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <form role="form" action="store.php" method="post" class="login-form">
                            <div class="form-group">
                                <label class="sr-only" for="form-username">Book Title</label>
                                <input type="text" name="book_title" placeholder="Book Title" class="form-username form-control" id="form-username">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-password">Author Name</label>
                                <input type="text" name="author_name" placeholder="Author Name" class="form-password form-control" id="form-password">
                            </div>
                            <button type="submit" class="btn">ADD!</button>
                        </form>
                    </div>
                </div>
            </div>

                </div>
            </div>
        </div>
    </div>

</div>



</body>

</html>