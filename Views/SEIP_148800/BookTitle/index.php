
<head>

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resourse/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resourse/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resourse/assets/css/">
    <link rel="stylesheet" href="../../../resourse/assets/css/style.css">
    <script src="../../../resourse/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resourse/assets/js/jquery-1.11.1.min.js"></script>

    </head>

<?php

require_once("../../../vendor/autoload.php");
use App\BookTitle\BookTitle;
use App\Message\Message;


$objBookTitle = new BookTitle();

$allData = $objBookTitle->index("obj");
$serial = 1;
echo "<table border='5px' align='center' >";

echo "<th> Serial </th>";
echo "<th> ID </th>";
echo "<th> Book Title </th>";
echo "<th> Author Name </th>";
echo "<th> Action </th>";


foreach($allData as $oneData){
    echo "<tr style='height: 40px'>";
    echo "<td>".$serial."</td>";

    echo "<td>".$oneData->id."</td>";
    echo "<td>".$oneData->book_title."</td>";
    echo "<td>".$oneData->author_name."</td>";


    echo "<td>";

    echo "<a href='view.php?id=$oneData->id'><button class='btn btn-info'>View</button></a> ";
    echo "<a href='edit.php?id=$oneData->id'><button class='btn btn-primary'>Edit</button></a> ";
    echo "<a href='delete.php?id=$oneData->id'><button class='btn btn-danger'>Delete</button></a> ";
    echo "<a href='trash.php?id=$oneData->id'><button class='btn btn-warning'>Trash</button></a> ";


    echo "</td>";

    echo "</tr>";

    $serial++;
}

echo "</table>";
