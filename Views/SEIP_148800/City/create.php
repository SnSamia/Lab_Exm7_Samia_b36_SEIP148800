<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

echo Message::message();
?>




<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resourse/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resourse/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resourse/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../resourse/assets/css/style.css">


</head>

<body>
<style>
    body{
        padding-top: 2px;

        background: url("../../../resourse/assets/img/backgrounds/city.jpg") no-repeat center center fixed;

        background-size:  cover;

    }
</style>


<div class="top-content">

    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text">
                    <h1><strong>ADD YOUR CITY  :)</strong> </h1>
                    <div class="description">

                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box">
                    <div class="form-top">
                        <div class="form-top-left">

                            <p>Enter your username and City Name:</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-home"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <form role="form" action="store.php" method="post" class="login-form">
                            <div class="form-group">
                                <label class="sr-only" for="form-username">Name</label>
                                <input type="text" name="name" placeholder="Name" class="form-username form-control" id="form-username">
                            </div>

                                <div class="form-group">
                                    <label class="sr-only" for="tt"></label>
                                    <select class="form-control input-sm" name="city_name">
                                        <option value="">Select an option</option>
                                        <option value="Dhaka">Dhaka</option>
                                        <option value="Chittagong">Chittagong</option>
                                        <option value="Rajshahi">Rajshahi</option>
                                        <option value="Feni">Feni</option>
                                        <option value="Barishal">Barishal</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn">ADD!</button>

                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>

</div>


<!-- Javascript -->
<script src="../../../resourse/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resourse/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../resourse/assets/js/jquery.backstretch.min.js"></script>
<script src="../../../resourse/assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../../resourse/assets/js/placeholder.js"></script>
<![endif]-->

</body>

</html>