
<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

echo Message::message();
?>




<!DOCTYPE html>
<html lang="en">

<head>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resourse/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resourse/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resourse/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../resourse/assets/css/style.css">


</head>

<body>

<style>
    body{
        padding-top: 10px;

        background: url("../../../resourse/assets/img/backgrounds/birthday1.jpg") no-repeat center center fixed;

        background-size: cover;

    }
</style>
<div class="top-content">

    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text">
                    <h1><strong>ADD HOBBIES  :)</strong> </h1>
                    <div class="description">

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box">
                    <div class="form-top">
                        <div class="form-top-left">

                            <p>Enter Your Username and Hobbies</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-film"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <form role="form" action="store.php" method="post" class="login-form">

                            <div class="panel-body">
                                <form role="form" action="store.php" method="post">
                                    <div class="form-group">
                                        <label for="tt"></label>
                                        <input type="text" name="name" id="name" class="form-control input-sm" placeholder="Enter Your Name">
                                    </div>
                                    <div class="form-group" style="text-align: left">
                                        <input type="checkbox" name="hobby1" id="hobby1"  value="Gardening">Gardening
                                    </div>
                                    <div class="form-group" style="text-align: left">
                                        <input type="checkbox" name="hobby2" id="hobby2" value="Reading Books">Reading Books
                                    </div>
                                    <div class="form-group" style="text-align: left">
                                        <input type="checkbox" name="hobby3" id="hobby3" value="Cooking">Cooking
                                    </div>
                                    <div class="form-group" style="text-align: left">
                                        <input type="checkbox" name="hobby4" id="hobby4" value="Traveling">Traveling
                                    </div><div class="form-group" style="text-align: left">
                                        <input type="checkbox" name="hobby4" id="hobby5" value="Shoppning">Shopping
                                    </div>
                            <button type="submit" class="btn">ADD !</button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>

</div>




</body>

</html>